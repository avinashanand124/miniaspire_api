## Mini Aspire API

# Installation Required

# PHP 7.4 and mysql

Mini Aspire REST API using auth tokens.

## Usage

#Clone the repository
```
git clone https://gitlab.com/avinashanand124/miniaspire_api.git

```

Change the *.env.example* to *.env* and add your database info

```
Run composer install
```
#create database miniaspire in mysql

```
create database miniaspire
```

#migrate database

```
php artisan migrate

```

# Run the webserver on port 8000
php artisan serve
```

## Routes

```
# Public Routes

POST   /api/login
@body: email, password

POST   /api/register
@body: name, email, password, password_confirmation


# Protected Routes

POST   /api/loans/apply
@body: amount, tenure

POST   /api/loans/pay-emi
@body: amount, loan_id


POST    /api/logout
```
{"mode":"full","isActive":false}

```

## Postman Collection
see [here](https://www.getpostman.com/collections/e56b91ebf59b3fcd48df)

## Run unit test

```
php artisan test --testsuite=Unit

```

