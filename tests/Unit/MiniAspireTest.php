<?php

namespace Tests\Unit;

use Tests\TestCase;

class MiniAspireTest extends TestCase
{
    /**
     * Test for require field
     */
    public function test_required_fields_for_registration()
    {
        $this->json('POST', 'api/register', ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "name" => ["The name field is required."],
                    "email" => ["The email field is required."],
                    "password" => ["The password field is required."],
                ]
            ]);
    }
    
    /**
     * Test repeat password
     */
    public function test_repeat_password()
    {
        $userData = [
            "name" => "Avinash Anand",
            "email" => "avinashanand124@gmail.com",
            "password" => "Test@123"
        ];

        $this->json('POST', 'api/register', $userData, ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "password" => ["The password confirmation does not match."]
                ]
            ]);
    }

    /**
     * Test successful registration
     */
    public function test_successful_registration()
    {
        $user = [
            "name" => "Dummy User",
            "email" => 'dummy'. rand(1,500).'@gmail.com',
            "password" => "Test@123",
            "password_confirmation" => "Test@123"
        ];

        $this->json('POST', 'api/register', $user, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJsonStructure([
                "user" => [
                    'id',
                    'name',
                    'email',
                    'created_at',
                    'updated_at',
                ],
                "token"
            ]);
    }
    
    /**
     * Email and password test
     */
    public function test_enter_email_and_password()
    {
        $this->json('POST', 'api/login')
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    'email' => ["The email field is required."],
                    'password' => ["The password field is required."],
                ]
            ]);
    }

    /**
     * Test Login
     */
    public function test_successful_login()
    {
        
        $loginDetails = ['email' => 'avinashanand1244@gmail.com', 'password' => 'Test@123'];

        $this->json('POST', 'api/login', $loginDetails, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJsonStructure([
               "user" => [
                   'id',
                   'name',
                   'email',
                   'email_verified_at',
                   'created_at',
                   'updated_at',
               ],
                "token"
            ]);
    }
    
    /**
     * Test auth token
     */
    public function test_auth_token_for_loan_apply()
    {
        $this->json('POST', 'api/loans/apply', ['Accept' => 'application/json','Authorization'=> 'Bearer 2|SSZk8NLUM8jhdUI97BQL3F6xz3rauhdGFvgySjKU'])
            ->assertStatus(401);
    }
    
    /**
     * Test Loan Apply
     */
    public function test_loan_apply()
    {
        $loans = [
            "amount" => 500000,
            "tenure" => 5
        ];
        $this->json('POST', 'api/loans/apply',$loans, ['Accept' => 'application/json','Authorization'=> "Bearer 45|nf07GG35PZBbWf2eC7DKxhx0qq71cGtByRb3miHL"])
            ->assertStatus(201)
            ->assertJsonStructure([
                    'id',
                    'customer_id',
                    'amount',
                    'tenure',
                    'interest_rate',
                    'status',
                    'created_at',
                    'updated_at'
            ]);
    }
    
    /**
     * Test EMI Payment
     */
    public function test_emi_payment()
    {
        $loanRepayment = [
            "amount" => 4940,
            "loan_id" => 1
        ];
        $this->json('POST', 'api/loans/pay-emi',$loanRepayment, ['Accept' => 'application/json','Authorization'=> "Bearer 45|nf07GG35PZBbWf2eC7DKxhx0qq71cGtByRb3miHL"])
            ->assertStatus(201)
            ->assertJsonStructure([
                    'id',
                    'loan_id',
                    'amount',
                    'status',
                    'created_at',
                    'updated_at'
            ]);
    }
}