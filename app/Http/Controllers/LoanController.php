<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Loan;
use App\Models\LoanRepayment;

class LoanController extends Controller
{
    
    protected $roi=10.5;
    
    /**
     * Apply for Loan
     * @param Request $request
     * @return type
     */
    public function applyLoan(Request $request){

        $request->validate([
           'amount' => 'required|numeric|min:1',
           'tenure' => 'required|numeric|min:1'
        ]);
        
        return Loan::create([
            'customer_id' =>auth()->id(),
            'amount' =>$request->input('amount'),
            'tenure' =>$request->input('tenure'),
            'interest_rate' => $this->roi,
            'status' => 'approved'
        ]);
        
    }
    
    /**
     * EMI Repayment Weekly
     * @param Request $request
     * @return type
     */
    public function emiRepayment(Request $request){
        
        $request->validate([
           'amount' => 'required|numeric|min:1',
           'loan_id' => 'required|numeric'
        ]);
        
        $borrowerDetail = auth()->user();
        
        if(empty($borrowerDetail)){
            return response(['message' => 'Invalid User'], 401);
        }
        
        $emiAmount = $request->input('amount');
        $loan_id = $request->input('loan_id');
        
        $loanDetails = Loan::find($loan_id)->first();
        
        if(empty($loanDetails))
            return response(['message' => 'Invalid loan account'], 400);
        
        $rate = $loanDetails['interest_rate']/(12*4.34*100);//calculate for weekly payment
        $term = $loanDetails['tenure']*12*4.34;
        $calculatedEmi = $loanDetails['amount'] * $rate * (pow(1 + $rate, $term) / (pow(1 + $rate, $term) - 1));
        
        
        if($emiAmount!=round($calculatedEmi))
            return response(['message' => 'Invalid amount. Your weekly emi='.round($calculatedEmi)], 400);
        
        return LoanRepayment::create([
            'amount' => round($calculatedEmi),
            'loan_id'=>$loanDetails['id'],
            'status' => 'success'
        ]);
        
    }
    
    
}
